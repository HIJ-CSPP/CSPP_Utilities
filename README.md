README
======
The package CSPP_Utilities provides some libraries, classes and actors.

Refer to https://git.gsi.de/EE-LV/CSPP/CSPP/wikis/home for CS++ project overview, details and documentation.

Related documents and information
---------------------------------
- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or D.Neidherr@gsi.de
- Download, bug reports... : 

GIT Submodules
==============
This package can be used as submodule.

- Packages/CSPP_Utilities

External Dependencies
=====================
- https://git.gsi.de/EE-LV/CSPP/CSPP_Core: Definition of CS++ ancestor classes


Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: https://eupl.eu/

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.